"""
The process of finding an input that satisfies a given predicate is called "solving." In Pylic, solving a predicate is framed as an optimization process, and so, solving a given predicate is the job of a numerical search algorithm (e.g., gradient descent).

Pylic includes compatibility layers for a few numerical search libraries. Integrating other libraries is straightforward.

"""
