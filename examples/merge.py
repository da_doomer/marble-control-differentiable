"""Script to merge JSONs from several runs into a single JSON."""
from pathlib import Path
from examples.plotting import Results
from examples.plotting import QualityResults
from examples.plotting import ExperimentID
from collections import defaultdict
import json


def merge_time_results(results: list[Results]) -> Results:
    """Merge the given results into a single results object."""
    merged_results: Results = defaultdict(lambda: defaultdict(list))

    # Add data from each result
    for result in results:
        for difficulty in result.keys():
            for solver_id in result[difficulty].keys():
                values = result[difficulty][solver_id]
                merged_results[difficulty][solver_id].extend(values)

    return merged_results


def merge_quality_result(quality_results: list[QualityResults]) -> QualityResults:
    """Merge the given quality results into a single results object."""
    merged = QualityResults()

    # Avoid assigning the same experiment ID to two experiments
    used_experiment_ids = set()

    # Add data from each result
    for qr in quality_results:
        # Local run experiment mapping
        experiment_ids = dict[ExperimentID, ExperimentID]()
        for entry in qr:
            experiment_id = entry[-1]

            # Check if this experiment ID has been reassigned already
            if experiment_id in experiment_ids.keys():
                new_id = experiment_ids[experiment_id]
            else:
                new_id = max(used_experiment_ids, default=0)+1
                experiment_ids[experiment_id] = new_id
                used_experiment_ids.add(new_id)

            new_entry = list(entry)
            new_entry[-1] = new_id

            merged.append(tuple(new_entry))

    return merged


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Plot experiment results')
    parser.add_argument('--time_jsons', type=Path, nargs="+", required=True)
    parser.add_argument('--quality_jsons', type=Path, nargs="+", required=True)
    parser.add_argument('--output_dir', type=Path, required=True)
    args = parser.parse_args()
    output_dir = args.output_dir
    output_dir.mkdir()

    # Open time results
    time_jsons = list()
    for time_json in args.time_jsons:
        with open(time_json, "rt") as fp:
            time_jsons.append(json.load(fp))

    # Get merged time results
    time_result = merge_time_results(time_jsons)

    # Write time JSON
    time_results_path = output_dir/"time_results.json"
    with open(time_results_path, "wt") as fp:
        fp.write(json.dumps(time_result, indent=2))
    print(f"Wrote {time_results_path}")

    # Open quality results
    quality_jsons = list()
    for quality_json in args.quality_jsons:
        with open(quality_json, "rt") as fp:
            quality_jsons.append(json.load(fp))

    # Get merged quality results
    quality_result = merge_quality_result(quality_jsons)

    # Write quality JSON
    quality_result_path = output_dir/"quality_results.json"
    with open(quality_result_path, "wt") as fp:
        fp.write(json.dumps(quality_result, indent=2))
    print(f"Wrote {quality_result_path}")
