from pathlib import Path
from plotting import plot_animation
from trajectory_pylic import pylic_solver
from trajectory_cem import cem_solver
from trajectory_cem import get_quality
from collections import defaultdict
from examples.plotting import QualityResults
from examples.plotting import Results
from examples.plotting import plot_results
from examples.plotting import plot_quality_results
import time
import json


output_dir = Path()/"half_cheetah_results"
repeat_n = 10


def main():
    output_dir.mkdir(exist_ok=False)

    # Solver with each solver
    solvers = [
        ("pylic", lambda: pylic_solver(
            depth_bound=40,
        )),
        ("cem", lambda: cem_solver(
            max_timesteps=512,
            timeout_s=3000,
        )),
    ]

    # Experiment results
    time_results: Results = defaultdict(lambda: defaultdict(list))
    quality_results: QualityResults = list()
    difficulty = 1  # there is only one difficulty
    experiment_id = 0

    # Write current results regularly
    time_results_path = output_dir/"results.json"
    quality_results_path = output_dir/"quality_results.json"

    def write_results(results: (Results | QualityResults), path: Path):
        with open(path, "wt") as fp:
            json.dump(results, fp, indent=2)
        print(f"Wrote {path}")

    for repeat_i in range(repeat_n):
        repeat_dir = output_dir/f"repeat_{repeat_i}"
        repeat_dir.mkdir()
        for solver_id, solver in solvers:
            experiment_id += 1
            solver_dir = repeat_dir/solver_id
            solver_dir.mkdir()

            # Run solver
            start_t = time.time()
            log, is_solved = solver()
            end_t = time.time()
            total_t = end_t - start_t

            # Log results
            time_results[difficulty][solver_id].append(total_t)
            for t, x in log:
                quality = get_quality(x)
                quality_results.append((
                    difficulty, solver_id, t, quality, experiment_id,
                ))
            write_results(time_results, time_results_path)
            write_results(quality_results, quality_results_path)

            # Write animation
            if is_solved:
                animation_path = solver_dir/"animation_SUCCESS.mp4"
            else:
                animation_path = solver_dir/"animation_FAIL.mp4"
            parameters = log[-1][1]
            plot_animation(
                parameters=parameters.tolist(),
                output_path=animation_path,
            )
            print(f"Wrote {animation_path}")

    # Write experiment results
    write_results(time_results, time_results_path)
    write_results(quality_results, quality_results_path)

    # Plot time results
    output_plots_dir = output_dir/"figures_time"
    output_plots_dir.mkdir()
    plot_results(time_results, output_plots_dir)
    print(f"Wrote {output_plots_dir}")

    # Plot quality results
    output_quality_plots_dir = output_dir/"figures_quality"
    output_quality_plots_dir.mkdir()
    plot_quality_results(quality_results, output_quality_plots_dir)


if __name__ == "__main__":
    main()
