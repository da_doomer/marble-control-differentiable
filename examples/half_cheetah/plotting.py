"""Plotting utilities for the ant environment."""
import tempfile
from pathlib import Path
import PIL.Image
import ffmpeg
import torch
import gymnasium as gym



def frames_to_video(frames, fps, output_path):
    with tempfile.TemporaryDirectory() as tdir:
        frame_dir = Path(tdir)
        for i, frame in enumerate(frames):
            path = frame_dir/(f"{i}.png").rjust(10)
            img = PIL.Image.fromarray(frame)
            img.save(path)
        (
            ffmpeg
            .input(frame_dir/"*.png", pattern_type="glob", framerate=fps)
            .output(str(output_path))
            .overwrite_output()
            .run(quiet=True)
        )


Action = tuple[float, float, float, float, float, float]


def plot_animation(
        parameters: list[Action],
        output_path: Path,
        ):
    """Plot the simulation induced by the given parameters as an animation."""
    env = gym.make("HalfCheetah-v4", render_mode="rgb_array", reset_noise_scale=0.0)
    frames = list()
    env.reset()
    for t in range(len(parameters)):
        action = parameters[t]
        env.step(action)
        frames.append(env.render())
    env.close()

    frames_to_video(
        frames=frames,
        fps=60,
        output_path=output_path,
    )
