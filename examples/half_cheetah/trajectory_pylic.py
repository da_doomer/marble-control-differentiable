"""Pylic to solve half-cheetah."""
from pylic.planner import concolic_planner
from pylic.planner import select_node_depth_first
from pylic.tape import Tape
from pylic.solvers import cma_es
from pylic.predicates import FunctionCall, Predicate
from pylic.predicates import Disjunction
from pylic.predicates import Conjunction
from pylic.predicates import LessThan
from pylic.predicates import Constants
from dataclasses import dataclass
import time
import math
import gymnasium as gym
import torch
import cma


# TODO: parametrize this magic constant (predicate episode length)
episode_len = 10


# Perform rollout
def simulator(parameters: torch.Tensor):
    env = gym.make("HalfCheetah-v4", reset_noise_scale=0.0)
    env.reset()
    for t in range(len(parameters)):
        action = parameters[t].tolist()
        observation, reward, terminated, _, info = env.step(action)
        contact_data = env.unwrapped.data.contact
        contact_pairs = list(zip(
            contact_data.geom1.copy(),
            contact_data.geom2.copy(),
        ))
    env.close()


def is_sat(parameters: torch.Tensor) -> bool:
    env = gym.make("HalfCheetah-v4", reset_noise_scale=0.0)
    env.reset()
    final_x_position = -math.inf
    for t in range(len(parameters)):
        action = parameters[t].tolist()
        _, __, terminated, ____, info = env.step(action)
        final_x_position = info["x_position"]
        if terminated:
            break
    env.close()
    print("final_x_position", final_x_position)
    return final_x_position > 8


# Attach start and end timesteps to the predicates, to make it easier
# to parse the decision variables for each solver call
@dataclass(frozen=True)
class TimedConjunction(Conjunction):
    operands: list[Predicate]
    start_t: int
    end_t: int


def get_next_predicates(
        tape: Tape,
        parameters: torch.Tensor,
        predicate: Predicate,
        ) -> list[Predicate]:
    # Predicate is always: next, once you land if you are in the air, try to
    # jump and have positive velocity
    # That is: at some point, `len(contact_pairs) == 0` and `info['x_velocity']
    # > 0.3`

    # First, identify first timestep of the last time the robot landed on both
    # feet
    last_land_start_t = 0
    start_position = 0
    prev_is_landed = False
    for node in tape:
        program_state = node.program_state
        if "info" not in program_state.keys():
            continue

        # Identify if the cheetah landed
        new_is_landed = len(program_state["contact_pairs"]) >= 2

        # If this is the first timestep of a new land, record the timestep
        if not prev_is_landed and new_is_landed:
            last_land_start_t = program_state["t"]
            start_position = program_state["info"]["x_position"]

        prev_is_landed = new_is_landed

    start_t = last_land_start_t + 1
    end_t = start_t + episode_len

    # Assemble predicate
    def get_incorrect_contacts(tape: Tape) -> torch.Tensor:
        # Return the number of contacts with the ground during the
        # timesteps in the "middle section" of the mini episode
        if start_t <= 1:
            return torch.tensor(0.0)

        # Define "middle section"
        min_t = start_t + episode_len//3
        max_t = end_t - episode_len//3

        incorrect_contacts = 0.0
        for node in tape:
            program_state = node.program_state
            if "contact_pairs" not in program_state.keys():
                continue
            if program_state["t"] > max_t:
                continue
            if program_state["t"] < min_t:
                continue
            contact_pairs = program_state["contact_pairs"]
            incorrect_contacts += len(contact_pairs)
        return torch.tensor(incorrect_contacts)

    # Assemble predicate
    def get_on_ground(t: int) -> Predicate:
        # Returns true if the half cheetah has both paws in the ground
        # at time t
        def is_on_ground(tape: Tape) -> torch.Tensor:
            for node in tape:
                program_state = node.program_state
                if "contact_pairs" not in program_state.keys():
                    continue
                if program_state["t"] != t:
                    continue
                contact_pairs = program_state["contact_pairs"]
                collision_geoms = [
                    i
                    for pair in contact_pairs
                    for i in pair
                ]
                is_on_ground = 5 in collision_geoms and 8 in collision_geoms
                if is_on_ground:
                    return torch.tensor(1.0)
            return torch.tensor(0.0)

        # Number of contacts < 1.0 iff number of contacts is 0
        return LessThan(
            torch.tensor(0.0),
            FunctionCall(is_on_ground, Constants.input_trace),
        )

    def get_final_position(tape: Tape) -> torch.Tensor:
        # Return true if cheetah moved at end of mini episode
        for node in tape:
            program_state = node.program_state
            if "info" not in program_state.keys():
                continue
            if program_state["t"] != end_t-1:
                continue
            position = program_state["info"]["x_position"]
            return torch.tensor(position)
        raise KeyError("Cannot get final position!")

    # Returns true if at timestep t there are wrong collisions
    def get_wrong_collision_n(tape: Tape) -> torch.Tensor:
        # Return number of wrong collisions (collisions with things other than
        # the paws)
        wrong_collision_n = 0
        for node in tape:
            program_state = node.program_state
            if "contact_pairs" not in program_state.keys():
                continue
            if start_t > program_state["t"] or end_t < program_state["t"]:
                continue
            contact_pairs = program_state["contact_pairs"]
            collision_geoms = [
                i
                for pair in contact_pairs
                for i in pair
            ]
            remaining_collisions = set(collision_geoms) - {0, 5, 8}
            is_wrong_collision = len(remaining_collisions) > 0
            if is_wrong_collision:
                wrong_collision_n += 1
        return wrong_collision_n

    min_progress = 0.4
    print(start_t, end_t)
    jump_predicate = TimedConjunction(
        [
            # Be airborne moving forward at some point
            #Disjunction([
            #    Conjunction([get_airborne(t), get_move_forward(t)])
            #    for t in range(start_t, end_t)
            #]),
            #LessThan(
            #    FunctionCall(get_incorrect_contacts, Constants.input_trace),
            #    torch.tensor(1.0),
            #),
            # At the end of this mini episode, position must have increased
            LessThan(
                torch.tensor(start_position+min_progress),
                FunctionCall(get_final_position, Constants.input_trace),
            ),
            # Only use paws to touch ground
            #LessThan(
            #    FunctionCall(get_wrong_collision_n, Constants.input_trace),
            #    torch.tensor(5.0),
            #),
            # Touch ground with paws at the end
            Disjunction([
                get_on_ground(t)
                for t in range(end_t-episode_len//3+1, end_t)
            ])
        ],
        start_t=start_t,
        end_t=end_t,
    )
    return [jump_predicate]


def solver(predicate: TimedConjunction, starting_parameters: torch.Tensor) -> torch.Tensor:
    if predicate is Constants.Top:
        return starting_parameters

    # TODO remove hardcoded magic
    opt_init = torch.tensor([
        [0.0 for _ in range(6)]
        for _ in range(predicate.end_t-predicate.start_t)
    ])
    prefix = starting_parameters.tolist()[:predicate.start_t]

    def candidate_processor(candidate: torch.Tensor) -> torch.Tensor:
        return torch.tensor(prefix + candidate.tolist())

    solution, _ = cma_es.solver(
        predicate=predicate,
        starting_parameters=opt_init,
        f=simulator,
        max_value=torch.tensor(10.0),
        custom_functions=dict(),
        custom_filters=dict(),
        initial_stdev=0.1,
        max_f_eval_n=10000,
        verbose=True,
        multiprocessing_workers=None,
        opts=cma.evolution_strategy.CMAOptions(
            tolfun=0.0,
            tolflatfitness=math.inf,
            tolfunhist=0.0,
        ),
        candidate_processor=candidate_processor,
    )
    return candidate_processor(solution)


def get_child_parameters(
        _: Tape,
        parameters: torch.Tensor,
        ) -> torch.Tensor:
    # We don't further process the output parameters
    return parameters


Parameters = torch.Tensor
LogTime = float
LogEntry = tuple[LogTime, Parameters]
Log = list[LogEntry]
IsSolved = bool


def pylic_solver(depth_bound: int) -> tuple[Log, IsSolved]:
    log = list()
    start_t = time.time()

    def log_call_to_solver(
            predicate: Predicate,
            starting_parameters: torch.Tensor,
            ) -> torch.Tensor:
        current_t = time.time() - start_t
        log.append((current_t, starting_parameters))
        return solver(
            predicate=predicate,
            starting_parameters=starting_parameters
        )

    # Call pylic
    try:
        parameters = concolic_planner(
            f=simulator,
            is_sat=is_sat,
            select_node=lambda t, e: select_node_depth_first(t, e, depth_bound),
            get_next_predicates=get_next_predicates,
            solver=log_call_to_solver,
            starting_parameters=torch.tensor([[0.0 for _ in range(6)]]),
            get_child_parameters=get_child_parameters,
            verbose=True,
        )
        current_t = time.time() - start_t
        log.append((current_t, parameters))
        return log, True
    except ValueError:  # thrown when planner runs out of nodes to explore
        return log, False
