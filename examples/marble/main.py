"""A simple example of using pylic over a marble simulation for trajectory
optimization in a navigation task.

This is a good example to see the minimum ingrediants needed to use Pylic:

- We have a Python simulation of the system.
- We can express the control task as a predicate over the execution of the simulator.

There three main steps:
1. Write the simulation as a program that takes the control parameters as input.
2. Describe the control task as a predicate.
3. Call the solver on the predicate and the simulation.

"""
from pathlib import Path
from pylic.solvers.gradient_descent import solver as solver
from pylic.predicates import Predicate
from pylic.predicates import Conjunction
from pylic.predicates import FunctionCall
from pylic.predicates import LessThan
from pylic.predicates import Filter
from pylic.predicates import IfOr
from pylic.predicates import Constants
from pylic.code_transformations import get_tracing_transformed_source
from tasks import example_task
from dynamics import State
from dynamics import normal_dynamics_force
from dynamics import collision_dynamics
from dynamics import collision_soft_predicate
from dynamics import get_trajectory
from plotting import plot_animation
import inspect
import torch

# Define the initial state
task = example_task
initial_state = task.initial_state


# Step 1: Write a simulation of the system that has to be controlled.
def simulation(
        actions: torch.Tensor,
        state: State,
        ) -> State:
    """Run the simulation with the given control
    signal and return the final state."""
    for t in range(len(actions)):
        # Compute next state with no collisions
        action = actions[t].clip(min=-1, max=1)*state.impulse_scale
        next_state = normal_dynamics_force(
            state,
            action,
            state.drag_constant,
            state.dt
        )

        # Check for collisions
        for i in range(len(state.segments)):
            v = collision_soft_predicate(state, state.dt, i)
            if v > 0: # ID: collision_check
                # If collided, adjust next state
                next_state = collision_dynamics(
                    state,
                    i,
                    state.dt,
                    state.coefficient_of_restitution
                )

        # Update state
        state = next_state
    return state


# Print the transformed version of the program
print(inspect.getsource(simulation))
print(get_tracing_transformed_source(inspect.getsource(simulation))[0])


# Step 2: Describe the control problem as a property of the execution of the simulator
predicate = Conjunction(list[Predicate]([
    # Collide with the obstacle during the
    # first half of the simulator
    # (collide(t=0) OR collide(t=1) OR ...)
    # We use a descriptive filter name to remember this
    IfOr(
        Filter(
            custom_filter="collide_half",
            trace=Constants.input_trace,
        )
    ),

    # And ensure the marble reaches the goal by ensuring
    # that the minimum distance to the goal is small.
    # We express "minimum distance to the goal" as a custom function.
    LessThan(
        left=FunctionCall(
            custom_function="min_distance_to_goal",
            trace=Constants.input_trace,
        ),
        right=torch.tensor(0.2),
    ),
]))

# Define the "collide_half" predicate. The function returns true
# for the nodes in the trace that deal with the third obstacle and were
# recorded in the first half of the simulation.
# These functions can also be defined inline, inside the predicates (see
# the ant example).
custom_filters = dict(
    collide_half=lambda tape, i:\
        "i" in tape[i].program_state.keys()\
        and tape[i].program_state["i"] == 1\
        and tape[i].program_state["t"] < task.max_timesteps//2\
        and tape[i].id == "collision_check"
)

# Define the "min_distance_to_goal" function
custom_functions = dict(
    min_distance_to_goal=lambda tape: min(
        (
            n.program_state["state"].marble.position - task.goal_circle.position
        ).norm()
        for n in tape
    )
)


# Step 3: Solve the predicate with pylic's gradient descent solver
starting_parameters = torch.zeros((task.max_timesteps, 2))
parameters, _ = solver(
    predicate,
    starting_parameters=starting_parameters,
    state=initial_state,
    f=simulation,
    max_value=torch.tensor(10),
    custom_filters=custom_filters,
    custom_functions=custom_functions,
    grad_mask=torch.ones_like(starting_parameters),
    iter_n=3000,
    learning_rate=0.1,
    momentum_beta=0.1,
    grad_norm_min=0.0001,
    normalize_gradient=True,
    dampening=0.999,
    line_search_scale=0.9,
    line_search_max_iter_n=5,
    max_consecutive_non_improvements=3000,
    max_consecutive_sat_improvements=0,
    verbose=True,
)

output_path = Path()/"marble_test"
output_path.mkdir()
output_animation=output_path/"output.mp4"
plot_animation(
    states=get_trajectory(parameters, initial_state),
    fps=60,
    goal_position=tuple(task.goal_circle.position.tolist()),
    goal_radius=task.goal_circle.radius,
    output_path=output_animation,
)
print(f"Wrote {output_animation}")
