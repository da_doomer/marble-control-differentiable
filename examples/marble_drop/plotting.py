from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import pymunk.matplotlib_util
from pathlib import Path
from functools import reduce


def plot_space(space: pymunk.Space, output_file: Path, margin: float):
    """Plot the given space to the given output file."""
    fig = Figure()
    FigureCanvas(fig)
    ax = fig.add_subplot(111)
    options = pymunk.matplotlib_util.DrawOptions(ax)
    # Only draw the shapes, nothing else:
    options.flags = pymunk.SpaceDebugDrawOptions.DRAW_SHAPES
    space.debug_draw(options)
    bbs = [shape.bb for shape in space.shapes]
    bb = reduce(lambda b1, b2: b1.merge(b2), bbs)
    ax.set_xlim(bb.left-margin, bb.right+margin)
    ax.set_ylim(bb.bottom-margin, bb.top+margin)
    ax.set_aspect('equal')
    fig.savefig(output_file)
