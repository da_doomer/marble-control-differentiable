"""Simple statistic analysis of the results."""
from pathlib import Path


DifficultyValue = int
SolverID = str
SolverTime = float
Quality = float
ExperimentID = int
Results = dict[DifficultyValue, dict[SolverID, list[SolverTime | None]]]
QualityResults = list[tuple[
    DifficultyValue, SolverID, SolverTime, Quality, ExperimentID
]]


def write_time_results_analysis(results: Results, output_dir: Path):
    return


if __name__ == "__main__":
    import argparse
    import json
    parser = argparse.ArgumentParser(description='Analyze experiment results')
    parser.add_argument('--time_results_json', type=Path, required=True)
    parser.add_argument('--quality_results_json', type=Path, required=True)
    parser.add_argument('--output_dir', type=Path, required=True)
    args = parser.parse_args()
    args.output_dir.mkdir(exist_ok=True)

    # Open time CSV
    with open(args.time_results_json, "rt") as fp:
        raw_results = json.load(fp)

    # Format results object correctly
    results = dict()
    for difficulty, data in raw_results.items():
        results[int(difficulty)] = data

    # Time analysis dir
    time_output_dir = args.output_dir/"time_figures"
    time_output_dir.mkdir(exist_ok=True)

    # Open quality CSV
    with open(args.quality_results_json, "rt") as fp:
        quality_results = json.load(fp)

    # Quality analysis dir
    quality_output_dir = args.output_dir/"quality_figures"
    quality_output_dir.mkdir(exist_ok=True)

    # Analyze
    try:
        write_time_results_analysis(results, time_output_dir)
    except Exception:
        print(traceback.format_exc())
        pass
    try:
        write_quality_results_analysis(quality_results, quality_output_dir)
    except Exception:
        print(traceback.format_exc())
        pass
