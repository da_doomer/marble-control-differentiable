"""Environment definition for the Ant environment.

Robot definitions are from DeepMind's PyMJCF tutorial:
https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=UAMItwu8e1WR
"""
from dataclasses import dataclass
from typing import Union
from dm_control import mjcf
import numpy as np


BODY_RADIUS = 0.1
BODY_SIZE = (BODY_RADIUS, BODY_RADIUS, BODY_RADIUS / 2)
random_state = np.random.RandomState(42)


class Leg(object):
    """A 2-DoF leg with position actuators.

    Source:
    https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=gKny5EJ4uVzu
    """
    def __init__(self, length, rgba):
        self.model = mjcf.RootElement()

        # Defaults:
        self.model.default.joint.damping = 2
        self.model.default.joint.type = 'hinge'
        self.model.default.geom.type = 'capsule'
        self.model.default.geom.rgba = rgba  # Continued below...

        # Thigh:
        self.thigh = self.model.worldbody.add('body')
        self.hip = self.thigh.add('joint', axis=[0, 0, 1])
        self.thigh.add('geom', fromto=[0, 0, 0, length, 0, 0], size=[length/4])

        # Hip:
        self.shin = self.thigh.add('body', pos=[length, 0, 0])
        self.knee = self.shin.add('joint', axis=[0, 1, 0])
        self.shin.add('geom', fromto=[0, 0, 0, 0, 0, -length], size=[length/5])

        # Position actuators:
        self.model.actuator.add('position', joint=self.hip, kp=10)
        self.model.actuator.add('position', joint=self.knee, kp=10)


def make_creature(num_legs: int) -> mjcf.RootElement:
    """Constructs a creature with `num_legs` legs.

    Source:
    https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=SESlL_TidKHx
    """
    model = mjcf.RootElement()
    model.compiler.angle = 'radian'  # Use radians.
    rgba = random_state.uniform([0, 0, 0, 1], [1, 1, 1, 1])

    # Make the torso geom.
    model.worldbody.add(
        'geom',
        name='torso',
        type='ellipsoid',
        size=BODY_SIZE,
        rgba=rgba
    )

    # Attach legs to equidistant sites on the circumference.
    for i in range(num_legs):
        theta = 2 * i * np.pi / num_legs
        hip_pos = BODY_RADIUS * np.array([np.cos(theta), np.sin(theta), 0])
        hip_site = model.worldbody.add('site', pos=hip_pos, euler=[0, 0, theta])
        leg = Leg(length=BODY_RADIUS, rgba=rgba)
        hip_site.attach(leg.model)
    return model


@dataclass(frozen=True)
class Cylinder:
    x: float
    y: float
    r: float
    h: float


@dataclass(frozen=True)
class Box:
    x: float
    y: float
    w: float
    l: float
    h: float


def make_cylinder(cylinder: Cylinder) -> mjcf.RootElement:
    """Make a cylindrical objetct."""
    root = mjcf.RootElement()
    # Place button
    root.worldbody.add(
        'geom',
        type='cylinder',
        size=(cylinder.r, cylinder.h),
        rgba=[0.2, 0.5, 0.2, 1],
        pos=(cylinder.x, cylinder.y, 0),
    )
    return root


def make_box(box: Box) -> mjcf.RootElement:
    """Make a cylindrical objetct."""
    root = mjcf.RootElement()
    # Place button
    root.worldbody.add(
        'geom',
        type='box',
        size=(box.w, box.l, box.h),
        rgba=[0.2, 0.2, 0.2, 1],
        pos=(box.x, box.y, 0),
    )
    return root


def make_arena() -> mjcf.RootElement:
    """ Create a flat arena.

    Source:
    https://colab.research.google.com/github/deepmind/dm_control/blob/main/tutorial.ipynb#scrollTo=F7_Tx9P9U_VJ
    """
    arena = mjcf.RootElement()
    chequered = arena.asset.add(
        'texture',
        type='2d',
        builtin='checker',
        width=300,
        height=300,
        rgb1=[.2, .3, .4],
        rgb2=[.3, .4, .5]
    )
    grid = arena.asset.add(
        'material',
        name='grid',
        texture=chequered,
        texrepeat=[5, 5],
        reflectance=.2
    )
    arena.worldbody.add(
        'geom',
        type='plane',
        size=[2, 2, .1],
        material=grid
    )
    for x in [-2, 2]:
        arena.worldbody.add('light', pos=[x, -1, 3], dir=[-x, 1, -2])
    return arena


class AntSimulation:
    """Encapsulates the simulator state of an ant simulation. The ant is
    controlled through a time-varying parametric periodic signal.
    """
    def __init__(
            self,
            num_legs: int,
            animation_fps: Union[None, float],
            sub_step_s: float, # how much time to advance for each call to step
            ):
        # Create an arena
        arena = make_arena()

        distance = 5

        # Place button in the arena
        self.button = Cylinder(
            x=BODY_RADIUS*distance,
            y=BODY_RADIUS*distance,
            r=BODY_RADIUS,
            h=BODY_RADIUS/10,
        )
        self.button_root = make_cylinder(self.button)
        button_spawn = arena.worldbody.add('site')
        button_spawn.attach(self.button_root)

        # Place goal in the arena
        self.goal = Cylinder(
            x=BODY_RADIUS*distance,
            y=-BODY_RADIUS*distance,
            r=BODY_RADIUS,
            h=BODY_RADIUS/10,
        )
        goal = make_cylinder(self.goal)
        goal_spawn = arena.worldbody.add('site')
        goal_spawn.attach(goal)

        # Place walls around goal
        walls = [
            Box(
                x=self.goal.x,
                y=self.goal.y-2*self.goal.r,
                w=BODY_RADIUS*2,
                l=BODY_RADIUS*0.1,
                h=BODY_RADIUS*2,
            ),
            Box(
                x=self.goal.x,
                y=self.goal.y+2*self.goal.r,
                w=BODY_RADIUS*2,
                l=BODY_RADIUS*0.1,
                h=BODY_RADIUS*2,
            ),
            Box(
                x=self.goal.x+2*self.goal.r,
                y=self.goal.y,
                w=BODY_RADIUS*0.2,
                l=BODY_RADIUS*2,
                h=BODY_RADIUS*2,
            ),
            Box(
                x=self.goal.x-2*self.goal.r,
                y=self.goal.y,
                w=BODY_RADIUS*0.2,
                l=BODY_RADIUS*2,
                h=BODY_RADIUS*2,
            ),
        ]
        self.wall_bodies = list()
        for wall in walls:
            wall_body = make_box(wall)
            self.wall_bodies.append(wall_body)
            spawn = arena.worldbody.add('site')
            spawn.attach(wall_body)

        # Place creature in the arena.
        creature = make_creature(num_legs=num_legs)
        height = BODY_RADIUS*1.5
        spawn_pos = (-BODY_RADIUS*distance, 0, height)
        # To spawn in the button:
        #spawn_pos = (self.button.x, self.button.y, height)
        spawn_site = arena.worldbody.add('site', pos=spawn_pos, group=3)
        spawn_site.attach(creature).add('freejoint')

        # Initiate simulation
        self.physics = mjcf.Physics.from_mjcf_model(arena)

        # Prepare animation data structure
        self.animation_fps = animation_fps
        self.video = []

        # Keep track of the list of actuators
        self.actuators = []
        self.torsos = []
        self.torsos.append(creature.find('geom', 'torso'))
        self.actuators.extend(creature.find_all('actuator'))

        # Define sub-step number
        self.sub_step_s = sub_step_s

        self.reset()

    def reset(self):
        """Reset the simulation state to the beginning of the simulation."""
        self.physics.reset()

    def sub_step(self, actions: np.ndarray):
        """Step the simulation once.

        The given `actions` is an array of `len(self.actuators)` numbers
        between that 0 and 1 that define the phase of each actuator.

        There are two actuators per leg.
        """
        # Control signal frequency, phase, amplitude.
        freq = 5
        phase = 2 * np.pi * actions
        amp = 0.9
        action = amp * np.sin(freq * self.physics.data.time + phase)

        # Inject controls and step the physics.
        self.physics.bind(self.actuators).ctrl = action
        self.physics.step()

        # Record animation frame if appropriate
        if self.animation_fps and len(self.video) < self.physics.data.time*self.animation_fps:
            pixels = self.physics.render()
            self.video.append(pixels.copy())

    def step(self, actions: np.ndarray) -> list[tuple[float, float]]:
        """Step the simulation until `self.sub_step_s` seconds have elapsed.
        Return the torso horizontal positions recorded during the step.

        The given `actions` is an array of `len(self.actuators)` numbers
        between that 0 and 1 that define the phase of each actuator.

        There are two actuators per leg.
        """
        start_t = self.physics.data.time
        torso_horizontal_positions = list()
        while self.physics.data.time - start_t < self.sub_step_s:
            self.sub_step(actions)
            torso_horizontal_positions.append(self.torso_horizontal_position)
        return torso_horizontal_positions

    @property
    def simulation_time_s(self) -> float:
        """Return the current simulation time."""
        return self.physics.data.time

    @property
    def torso_horizontal_position(self) -> tuple[float, float]:
        """Return the horizontal position of the torso of the ant."""
        return (
            self.physics.bind(self.torsos).xpos[0, 0].copy(),
            self.physics.bind(self.torsos).xpos[0, 1].copy()
        )

    def activate_button(self):
        """Push the button."""
        # Change button color
        for geom in self.button_root.find_all('geom'):
            self.physics.bind(geom).rgba = ([0.5, 0.2, 0.2, 1])

        # Remove walls
        for wall_body in self.wall_bodies:
            for geom in wall_body.find_all('geom'):
                o = self.physics.bind(geom)
                o.pos = (o.pos[0], o.pos[1], -5)


def get_simulation(animation_fps: Union[float, None]) -> AntSimulation:
    """Return the simulation that defines the ant task."""
    num_legs = 4
    sub_step_s = 2
    env = AntSimulation(
        num_legs=num_legs,
        animation_fps=animation_fps,
        sub_step_s=sub_step_s,
    )
    return env
